playSound = function(file_name) //id in the sounds[i] array., vol is a real number in the [0, 1] interval
{
    var a = new Audio( file_name );
    a.load()
    a.volume = 1;
    a.play();
}
